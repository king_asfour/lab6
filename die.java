import java.util.Random;
public class die{
	private int pips;
	private Random randomness;
	
	public die(){
		pips = 1;
		randomness = new Random();
	}
	
	public int getPips(){
		return this.pips;
	}
	public Random getRandomness(){
		return this.randomness;
	}
	public void roll(){
		this.pips = randomness.nextInt(6) + 1; 
	}
	
	public String toString(){
		return "The number on the die is: " + this.pips;
	}
}