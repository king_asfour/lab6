public class Board{
	private die d1;
	private die d2;
	private boolean[] closedTiles;
	
	public Board(){
		this.d1 = new die();
		this.d2 = new die();
		
		this.closedTiles = new boolean [12];
	}
	
	public String toString(){
		String tiles = " ";
		int count = 1;
		for(int i = 0; i < closedTiles.length; i++){
			if(closedTiles[i] == false){
				tiles = tiles + count;
 			}else{
				tiles = tiles + "X";
			}
			count++;
		}
		return tiles;
	}
	
	public boolean playATurn(){
		this.d1.roll();
		this.d2.roll();
		
		System.out.println(this.d1.getPips());
		System.out.println(this.d2.getPips());
		
		int sum = this.d1.getPips() + this.d2.getPips();
		
		if(closedTiles[sum - 1] == false){
			closedTiles[sum - 1] = true;
			System.out.println("Closing tile: " + sum);
			return false;
		}else{
			System.out.println("This tile is already shut");
			return true;
		}
	}
}