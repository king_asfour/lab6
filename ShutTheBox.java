public class ShutTheBox{
	public static void main (String [] args){
		System.out.println("WELCOME TO SHUT THE BOX! GREATEST GAME EVER!!");
		boolean gameOver = false;
		
		Board gameBoard = new Board();
		
		while(gameOver == false){
			System.out.println("Player 1's turn");
			System.out.println(gameBoard);
			
			if(gameBoard.playATurn() == true){
				System.out.println("Player 2 won!!!");
				gameOver = true;
				break;
			}else{
				System.out.println("Player 2's turn");
				System.out.println(gameBoard);
			}
			if(gameBoard.playATurn() == true){
				System.out.println("Player 1 WON !!");
				gameOver = true;
			}else{
				continue;
			}
		}
	}
}